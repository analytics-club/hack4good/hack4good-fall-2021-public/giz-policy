# GIZ Policy 
## Jina Example

This is a working example for jina.
So far it was just implemented with the first 200 lines from the Climate Change Whitepaper of South Africa. But you can easily increase the number of lines & documents.

### Requirements
You just need docker installed on your machine. 
What is docker -> https://www.docker.com/
Once docker is downloaded open the dashboard and adjust the settings. The important thing is to adjust the default Memory size to 8 GB as shown below. Click on Apply & Restart once adjusted.
![Docker-Settings](data/docker_settings.png)

### Step 0: Create the Docker image
Move with your console (Terminal/Powershell) to the ```giz-policy/code``` folder: ```cd YourPath/giz-policy/code```

Run:
Create a docker image with this command. (Dont forget the trailing dot)
```docker build -t giz-policy .```

Run the image in interactive mode.
```docker run -ti giz-policy bash```


### Step 1: Indexing of the Data
Before we can perform neural search the data input has to be indexed. That means each sentence is transformed into a vector (embedding) which represents the semantic meaning of a sentence in a vector space.

Run:

```python jina_example.py -t index```

This will create a new folder in the code folder called `workspace`. This is where the embeddings are stored.

### Step 2: Query Your Data
Now that you ran the indexing you can query your data. The query is translated as well into the vector space. The best results are those vectors that are closest to the query vector.

Run:

```python jina_example.py -t query```

You will have to provide a query in the command line, as shown in the snapshot below, e.g. **What is Climate Change?** The results are printed below with the corresponding cosine-similarity score.

![Terminal-Snapshot](data/jina_example.png)